--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: SequelizeMeta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


ALTER TABLE public."SequelizeMeta" OWNER TO postgres;

--
-- Name: orderitems; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.orderitems (
    id integer NOT NULL,
    id_product integer,
    id_user integer,
    amount integer,
    total_price integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.orderitems OWNER TO postgres;

--
-- Name: orderitems_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.orderitems_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.orderitems_id_seq OWNER TO postgres;

--
-- Name: orderitems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.orderitems_id_seq OWNED BY public.orderitems.id;


--
-- Name: payments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.payments (
    id integer NOT NULL,
    id_order integer,
    payment_method character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.payments OWNER TO postgres;

--
-- Name: payments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.payments_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payments_id_seq OWNER TO postgres;

--
-- Name: payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.payments_id_seq OWNED BY public.payments.id;


--
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    id integer NOT NULL,
    name_product character varying(255),
    price integer,
    "desc" text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.products OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- Name: profiles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.profiles (
    id integer NOT NULL,
    username character varying(255),
    email character varying(255),
    password character varying(255),
    id_game integer,
    server_game integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public.profiles OWNER TO postgres;

--
-- Name: profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.profiles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.profiles_id_seq OWNER TO postgres;

--
-- Name: profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.profiles_id_seq OWNED BY public.profiles.id;


--
-- Name: orderitems id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orderitems ALTER COLUMN id SET DEFAULT nextval('public.orderitems_id_seq'::regclass);


--
-- Name: payments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments ALTER COLUMN id SET DEFAULT nextval('public.payments_id_seq'::regclass);


--
-- Name: products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- Name: profiles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profiles ALTER COLUMN id SET DEFAULT nextval('public.profiles_id_seq'::regclass);


--
-- Data for Name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."SequelizeMeta" (name) FROM stdin;
20220426024334-create-profile.js
20220426025148-create-orderitem.js
20220426025455-create-products.js
20220426025555-create-payment.js
\.


--
-- Data for Name: orderitems; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.orderitems (id, id_product, id_user, amount, total_price, "createdAt", "updatedAt") FROM stdin;
1	1	2	3	6000	2022-04-27 01:52:45.091+07	2022-04-27 01:52:45.091+07
2	2	1	2	5000	2022-04-27 01:54:25.144+07	2022-04-27 01:54:25.144+07
3	1	3	2	4000	2022-04-27 01:54:50.431+07	2022-04-27 01:54:50.431+07
4	4	4	1	10000	2022-04-27 01:55:21.594+07	2022-04-27 01:55:21.594+07
5	5	5	1	13500	2022-04-27 01:56:35.881+07	2022-04-27 01:56:35.881+07
\.


--
-- Data for Name: payments; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.payments (id, id_order, payment_method, "createdAt", "updatedAt") FROM stdin;
1	1	OVO	2022-04-27 01:58:42.101+07	2022-04-27 01:58:42.101+07
2	2	OVO	2022-04-27 01:58:52.04+07	2022-04-27 01:58:52.04+07
3	3	DANA	2022-04-27 01:59:00.479+07	2022-04-27 01:59:00.479+07
4	4	gopay	2022-04-27 01:59:15.267+07	2022-04-27 01:59:15.267+07
5	5	gopay	2022-04-27 01:59:18.404+07	2022-04-27 01:59:18.404+07
\.


--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (id, name_product, price, "desc", "createdAt", "updatedAt") FROM stdin;
1	Diamond 3 - MLBB	2000	Pembelian diamond 3 untuk mobile legends bang bang	2022-04-27 01:45:49.16+07	2022-04-27 01:46:25.612+07
2	Diamond 5 - MLBB	2500	Pembelian diamond 5 untuk mobile legends bang bang	2022-04-27 01:47:00.589+07	2022-04-27 01:47:00.589+07
3	Diamond 12 - MLBB	4000	Pembelian diamond 12 untuk mobile legends bang bang	2022-04-27 01:47:22.169+07	2022-04-27 01:47:22.169+07
4	Diamond 36 - MLBB	10000	Pembelian diamond 36 untuk mobile legends bang bang	2022-04-27 01:48:22.57+07	2022-04-27 01:48:22.57+07
5	Diamond 50 - MLBB	13500	Pembelian diamond 50 untuk mobile legends bang bang	2022-04-27 01:48:52.697+07	2022-04-27 01:56:10.675+07
\.


--
-- Data for Name: profiles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.profiles (id, username, email, password, id_game, server_game, "createdAt", "updatedAt") FROM stdin;
1	Aniisah	aniisah@gmail.com	aniisah123	123456	654321	2022-04-27 01:27:16.155+07	2022-04-27 01:36:05.446+07
2	Rizki	rizki@gmail.com	rizki123	789121	134787	2022-04-27 01:31:10.46+07	2022-04-27 01:36:59.613+07
3	Agung	agung@gmail.com	agung123	829341	237891	2022-04-27 01:37:59.605+07	2022-04-27 01:37:59.605+07
4	Jaya	jaya@gmail.com	jaya123	934567	456781	2022-04-27 01:38:38.522+07	2022-04-27 01:38:38.522+07
5	Ghany	ghany@gmail.com	ghany123	569021	378012	2022-04-27 01:39:17.984+07	2022-04-27 01:39:17.984+07
6	Rama	rama@gmail.com	$2a$10$6RVZ1VH/Z2l1CTfYyt.fz.PCRNegCvQBXENTTtkQ8h8/6.jfoLG/2	156792	765421	2022-04-28 11:32:10.948+07	2022-04-28 11:32:10.948+07
7	Haha	haha@gmail.com	$2a$10$ySqEF1ji9mZ0hyhMb8t9jev8DoCV3JmbN/gs19Kl5t5Ol2f7t0wtK	213890	123678	2022-04-28 12:31:45.017+07	2022-04-28 12:31:45.017+07
8	Rina	rina@gmail.com	$2a$10$uS/1cYLHX29SXs64PdhvaOJLVOUOWEO5LLc.yNKGdtkKEsZvQiWRG	321890	453681	2022-04-28 21:11:37.675+07	2022-04-28 21:11:37.675+07
9	Cindy	cindy@gmail.com	$2a$10$7kwNGjQXj2tCQz8gw..MnO4LoVgDBp15f/ePH2crc5jim.6jxTi9m	543218	346721	2022-04-28 21:32:56.208+07	2022-04-28 21:32:56.208+07
\.


--
-- Name: orderitems_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.orderitems_id_seq', 5, true);


--
-- Name: payments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.payments_id_seq', 6, true);


--
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 5, true);


--
-- Name: profiles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.profiles_id_seq', 9, true);


--
-- Name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- Name: orderitems orderitems_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.orderitems
    ADD CONSTRAINT orderitems_pkey PRIMARY KEY (id);


--
-- Name: payments payments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (id);


--
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- Name: profiles profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

