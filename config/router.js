const express = require("express");
const controllers = require("../app/controllers");

const apiRouter = express.Router();

/**
 * TODO: Implement your own API
 *       implementations
 */
 apiRouter.post("/api/v1/profile", controllers.api.v1.profileController.create);
 apiRouter.post("/api/v1/profile/login", controllers.api.v1.profileController.login);

apiRouter.get("/api/v1/profile",controllers.api.v1.profileController.authorize,controllers.api.v1.profileController.list);
apiRouter.put("/api/v1/profile/:id", controllers.api.v1.profileController.authorize,controllers.api.v1.profileController.update);
apiRouter.get("/api/v1/profile/:id", controllers.api.v1.profileController.authorize,controllers.api.v1.profileController.show);
apiRouter.get("/api/v1/profiles",controllers.api.v1.profileController.authorize,controllers.api.v1.profileController.findAlllist);
apiRouter.delete("/api/v1/profile/:id",controllers.api.v1.profileController.authorize,controllers.api.v1.profileController.destroy);

apiRouter.get("/api/v1/orderitem", controllers.api.v1.profileController.authorize,controllers.api.v1.orderitemController.list);
apiRouter.post("/api/v1/orderitem", controllers.api.v1.profileController.authorize,controllers.api.v1.orderitemController.create);
apiRouter.put("/api/v1/orderitem/:id", controllers.api.v1.profileController.authorize,controllers.api.v1.orderitemController.update);
apiRouter.get("/api/v1/orderitem/:id", controllers.api.v1.profileController.authorize,controllers.api.v1.orderitemController.show);
apiRouter.get("/api/v1/orderitems", controllers.api.v1.profileController.authorize,controllers.api.v1.orderitemController.findAlllist);
apiRouter.delete("/api/v1/orderitem/:id",controllers.api.v1.profileController.authorize,controllers.api.v1.orderitemController.destroy
);

apiRouter.get("/api/v1/products", controllers.api.v1.profileController.authorize,controllers.api.v1.productsController.list);
apiRouter.post("/api/v1/products", controllers.api.v1.profileController.authorize,controllers.api.v1.productsController.create);
apiRouter.put("/api/v1/products/:id", controllers.api.v1.profileController.authorize,controllers.api.v1.productsController.update);
apiRouter.get("/api/v1/products/:id",controllers.api.v1.profileController.authorize, controllers.api.v1.productsController.show);
apiRouter.get("/api/v1/productss", controllers.api.v1.profileController.authorize,controllers.api.v1.productsController.findAlllist);
apiRouter.delete("/api/v1/products/:id",controllers.api.v1.profileController.authorize,controllers.api.v1.productsController.destroy);

apiRouter.get("/api/v1/payment", controllers.api.v1.profileController.authorize,controllers.api.v1.paymentController.list);
apiRouter.post("/api/v1/payment",controllers.api.v1.profileController.authorize, controllers.api.v1.paymentController.create);
apiRouter.put("/api/v1/payment/:id",controllers.api.v1.profileController.authorize, controllers.api.v1.paymentController.update);
apiRouter.get("/api/v1/payment/:id",controllers.api.v1.profileController.authorize, controllers.api.v1.paymentController.show);
apiRouter.get("/api/v1/payments", controllers.api.v1.profileController.authorize,controllers.api.v1.paymentController.findAlllist);
apiRouter.delete("/api/v1/payment/:id",controllers.api.v1.profileController.authorize,controllers.api.v1.paymentController.destroy);



/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */
apiRouter.get("/api/v1/errors", () => {
  throw new Error(
    "The Industrial Revolution and its consequences have been a disaster for the human race."
  );
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
