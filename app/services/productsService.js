const productsRepository = require("../repositories/productsRepository");

module.exports = {
  create(requestBody) {
    return productsRepository.create(requestBody);
  },

  update(id, requestBody) {
    return productsRepository.update(id, requestBody);
  },

  delete(id) {
    return productsRepository.delete(id);
  },

  async list() {
    try {
      const posts = await productsRepository.findAll();
      // const postCount = await productsRepository.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  async findAlllist() {
    try {
      const posts = await productsRepository.findAll();
      // const postCount = await userRepo.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return productsRepository.find(id);
  },
};
