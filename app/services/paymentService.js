const paymentRepository = require("../repositories/paymentRepository");

module.exports = {
  create(requestBody) {
    return paymentRepository.create(requestBody);
  },

  update(id, requestBody) {
    return paymentRepository.update(id, requestBody);
  },

  delete(id) {
    return paymentRepository.delete(id);
  },

  async list() {
    try {
      const posts = await paymentRepository.findAll();
      // const postCount = await paymentRepository.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  async findAlllist() {
    try {
      const posts = await paymentRepository.findAll();
      // const postCount = await userRepo.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return paymentRepository.find(id);
  },
};
