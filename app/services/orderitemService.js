const orderitemRepository = require("../repositories/orderitemRepository");

module.exports = {
  create(requestBody) {
    return orderitemRepository.create(requestBody);
  },

  update(id, requestBody) {
    return orderitemRepository.update(id, requestBody);
  },

  delete(id) {
    return orderitemRepository.delete(id);
  },

  async list() {
    try {
      const posts = await orderitemRepository.findAll();
      // const postCount = await orderitemRepository.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  async findAlllist() {
    try {
      const posts = await orderitemRepository.findAll();
      // const postCount = await userRepo.getTotalPost();

      return {
        data: posts,
      };
    } catch (err) {
      throw err;
    }
  },

  get(id) {
    return orderitemRepository.find(id);
  },
};
