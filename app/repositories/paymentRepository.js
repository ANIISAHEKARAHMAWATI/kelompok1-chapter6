const { payment,orderitem } = require("../models");

module.exports = {
  create(createArgs) {
    return payment.create(createArgs);
  },

  update(id, updateArgs) {
    return payment.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return payment.destroy(id);
  },

  find(id) {
    return payment.findByPk(id,{
      attributes: ["id", "id_order", "payment_method"],
      include :[
        {
          model:orderitem,
          attributes: ["id", "id_product", "id_user", "amount","total_price"],
          // as:"products",
        },
       
      ],
    });
  },

  findAll() {
    return payment.findAll({
      attributes: ["id", "id_order", "payment_method"],
      include :[
        {
          model:orderitem,
          attributes: ["id", "id_product", "id_user", "amount","total_price"],
          // as:"products",
        },
       
      ],
    });
  },

  getTotalorder() {
    return payment.count();
  },
};
