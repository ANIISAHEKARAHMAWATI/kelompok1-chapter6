const { profile } = require("../models");

module.exports = {
  create(createArgs) {
    return profile.create(createArgs);
  },

  findemail(email) {
    return profile.findOne({
      where: { email },
    });
  },

  update(id, updateArgs) {
    return profile.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return profile.destroy(id);
  },

  find(id) {
    return profile.findByPk(id);
  },

  findAll() {
    return profile.findAll();
  },

  getTotalprofile() {
    return profile.count();
  },
};
