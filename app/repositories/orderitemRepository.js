const { orderitem,products,profile } = require("../models");

module.exports = {
  create(createArgs) {
    return orderitem.create(createArgs);
  },

  update(id, updateArgs) {
    return orderitem.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return orderitem.destroy(id);
  },

  find(id) {
    return orderitem.findByPk(id,{
      attributes: ["id", "id_product", "id_user","amount","total_price"],
      include :[
        {
          model:products,
          attributes: ["id", "name_product", "price", "desc"],
          // as:"products",
        },
        {
          model:profile,
          attributes: ["id", "username", "email", "password","id_game","server_game"],
          // as:"profile",
        },
      ],
    });
  },

  findAll() {
    return orderitem.findAll({
      attributes: ["id", "id_product", "id_user","amount","total_price"],
      include :[
        {
          model:products,
          attributes: ["id", "name_product", "price", "desc"],
          // as:"products",
        },
        {
          model:profile,
          attributes: ["id", "username", "email", "password","id_game","server_game"],
          // as:"profile",
        },
      ],
    });
  },

  getTotalorder() {
    return orderitem.count();
  },
};
