const { products } = require("../models");

module.exports = {
  create(createArgs) {
    return products.create(createArgs);
  },

  update(id, updateArgs) {
    return products.update(updateArgs, {
      where: {
        id,
      },
    });
  },

  delete(id) {
    return products.destroy(id);
  },

  find(id) {
    return products.findByPk(id);
  },

  findAll() {
    return products.findAll();
  },

  getTotalorder() {
    return products.count();
  },
};
