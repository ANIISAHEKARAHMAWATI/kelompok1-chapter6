'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class orderitem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // Option 1

    orderitem.belongsTo(models.products,{
      foreignKey:"id_product"
    });

    orderitem.belongsTo(models.profile,{
      foreignKey:"id_user"
    });
    }
  }


  orderitem.init({
    id_product:DataTypes.INTEGER,
    id_user:DataTypes.INTEGER,
    amount: DataTypes.INTEGER,
    total_price: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'orderitem',
  });
  return orderitem;
};