const profileController = require("./profileController");
const orderitemController = require("./orderitemController");
const productsController = require("./productsController");
const paymentController = require("./paymentController");

module.exports = {
  profileController,
  orderitemController,
  productsController,
  paymentController,
};
