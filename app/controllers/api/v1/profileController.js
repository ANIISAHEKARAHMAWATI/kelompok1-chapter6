const profileService = require("../../../services/profileService");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { profile } = require("../../../models");
const SALT = 10;

function encryptPassword(password) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, SALT, (err, encryptedPassword) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(encryptedPassword);
    });
  });
}

function checkPassword(password, inputpassword) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(inputpassword, password, (err, isPasswordCorrect) => {
      if (!!err) {
        reject(err);
        return;
      }

      resolve(isPasswordCorrect);
    });
  });
}

function createToken(payload) {
  return jwt.sign(payload, process.env.JWT_SIGNATURE_KEY || "Rahasia");
}


module.exports = {
  findAlllist(req,res){
    profileService
    .findAlllist()
    .then(({ data }) => {
      res.status(200).json({
        status: "OK",
        data: { posts: data },
      });
    })
    .catch((err) => {
      res.status(400).json({
        status: "FAIL",
        message: err.message,
      });
    });
  },

  list(req, res) {
    profileService
      .list()
      .then(({ data, count }) => {
        res.status(200).json({
          status: "OK",
          data: { posts: data },
          meta: { total: count },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

 async create(req, res) {
    const username = req.body.username;
    const email = req.body.email;
    const id_game = req.body.id_game;
    const server_game = req.body.server_game;
    const password = await encryptPassword(req.body.password);
    const data = {username, email, id_game, server_game, password};
    const searchemail= await profileService.findemail(email);
    if(searchemail){
      res.status(404).json({
        message: "Email telah digunakan"
      })
      return
    }
      profileService
      .create(data)
      .then((post) => {
        res.status(201).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
    
  },

  async login(req, res) {
    const email = req.body.email.toLowerCase(); // Biar case insensitive
    const inputpassword = req.body.password;

    const user = await profileService.findemail(email)

    if (!user) {
      res.status(404).json({ message: "Email tidak ditemukan" });
      return;
    }

    const isPasswordCorrect = await checkPassword(
      user.password,
      inputpassword
    );

    if (!isPasswordCorrect) {
      res.status(401).json({ message: "Password salah!" });
      return;
    }

    const token = createToken({
      id: user.id,
      username: user.username,
      email: user.email,
      id_game:user.id_game,
      server_game:user.server_game,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });

    res.status(201).json({
      id: user.id,
      email: user.email,
      token,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
    });
  },

 async update(req, res) {
    const username = req.body.username;
    const email = req.body.email;
    const id_game = req.body.id_game;
    const server_game = req.body.server_game;
    const password = await encryptPassword(req.body.password);
    const data = {username, email, id_game, server_game, password};
    const searchuser = await profileService.get(req.params.id);
    if(!searchuser){
      res.status(404).json({
        message: "User tidak ditemukan"
      })
      return
    }
    profileService
      .update(req.params.id, data)
      .then(() => {
        res.status(200).json({
          status: "OK",
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  show(req, res) {
    profileService
      .get(req.params.id)
      .then((post) => {
        res.status(200).json({
          status: "OK",
          data: post,
        });
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  destroy(req, res) {
    
    profileService
      .delete({ where: {
        id: req.params.id }
      })
      .then(() => {
        res.status(204).end();
      })
      .catch((err) => {
        res.status(422).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  async authorize(req, res, next) {
    try {
      const bearerToken = req.headers.authorization;
      const token = bearerToken.split("Bearer ")[1];
      const tokenPayload = jwt.verify(
        token,
        process.env.JWT_SIGNATURE_KEY || "Rahasia"
      );

      req.user = await profileService.get(tokenPayload.id);
      next();
    } catch (err) {
      console.error(err);
      res.status(401).json({
        message: "Unauthorized",
      });
    }
  },

};
